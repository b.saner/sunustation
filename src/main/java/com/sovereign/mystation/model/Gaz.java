package com.sovereign.mystation.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "gaz")
public class Gaz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;


    @Column(name = "nom27", columnDefinition = "varchar(255) default '2.7'")
    private String nom27;
    @Column(name= "prix27", columnDefinition = "integer default 2885")
    private int prix27;
    @Column(name = "statut27", columnDefinition = "integer default 3")
    private int statut27;

    @Column(name = "nom6", columnDefinition = "varchar(255) default '6'")
    private String nom6;
    @Column(name= "prix6", columnDefinition = "integer default 2885")
    private int prix6;
    @Column(name = "statut6", columnDefinition = "integer default 3")
    private int statut6;

    @Column(name = "nom9", columnDefinition = "varchar(255) default '9'")
    private String nom9;
    @Column(name= "prix9", columnDefinition = "integer default 4285")
    private int prix9;
    @Column(name = "statut9", columnDefinition = "integer default 3")
    private int statut9;

    @Column(name = "nom125", columnDefinition = "varchar(255) default '12.5'")
    private String nom125;
    @Column(name= "prix125", columnDefinition = "integer default 6250")
    private int prix125;
    @Column(name = "statut125", columnDefinition = "integer default 3")
    private int statut125;

    @Column(name = "nom32", columnDefinition = "varchar(255) default '32'")
    private String nom32;
    @Column(name= "prix32", columnDefinition = "integer default 16000")
    private int prix32;
    @Column(name = "statut32", columnDefinition = "integer default 3")
    private int statut32;

    @Column(name = "nom38", columnDefinition = "varchar(255) default '38'")
    private String nom38;
    @Column(name= "prix38", columnDefinition = "integer default 19000")
    private int prix38;
    @Column(name = "statut38", columnDefinition = "integer default 3")
    private int statut38;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom27() {
        return nom27;
    }

    public void setNom27(String nom27) {
        this.nom27 = nom27;
    }

    public int getPrix27() {
        return prix27;
    }

    public void setPrix27(int prix27) {
        this.prix27 = prix27;
    }

    public int getStatut27() {
        return statut27;
    }

    public void setStatut27(int statut27) {
        this.statut27 = statut27;
    }

    public String getNom6() {
        return nom6;
    }

    public void setNom6(String nom6) {
        this.nom6 = nom6;
    }

    public int getPrix6() {
        return prix6;
    }

    public void setPrix6(int prix6) {
        this.prix6 = prix6;
    }

    public int getStatut6() {
        return statut6;
    }

    public void setStatut6(int statut6) {
        this.statut6 = statut6;
    }

    public String getNom9() {
        return nom9;
    }

    public void setNom9(String nom9) {
        this.nom9 = nom9;
    }

    public int getPrix9() {
        return prix9;
    }

    public void setPrix9(int prix9) {
        this.prix9 = prix9;
    }

    public int getStatut9() {
        return statut9;
    }

    public void setStatut9(int statut9) {
        this.statut9 = statut9;
    }

    public String getNom125() {
        return nom125;
    }

    public void setNom125(String nom125) {
        this.nom125 = nom125;
    }

    public int getPrix125() {
        return prix125;
    }

    public void setPrix125(int prix125) {
        this.prix125 = prix125;
    }

    public int getStatut125() {
        return statut125;
    }

    public void setStatut125(int statut125) {
        this.statut125 = statut125;
    }

    public String getNom32() {
        return nom32;
    }

    public void setNom32(String nom32) {
        this.nom32 = nom32;
    }

    public int getPrix32() {
        return prix32;
    }

    public void setPrix32(int prix32) {
        this.prix32 = prix32;
    }

    public int getStatut32() {
        return statut32;
    }

    public void setStatut32(int statut32) {
        this.statut32 = statut32;
    }

    public String getNom38() {
        return nom38;
    }

    public void setNom38(String nom38) {
        this.nom38 = nom38;
    }

    public int getPrix38() {
        return prix38;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getStatut() {
        return statut;
    }

    public void setPrix38(int prix38) {
        this.prix38 = prix38;
    }

    public int getStatut38() {
        return statut38;
    }

    public void setStatut38(int statut38) {
        this.statut38 = statut38;
    }
}

package com.sovereign.mystation.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class GazDetail implements Serializable {

    @Column(name = "nom")
    private String nom;

    @Column(name= "prix")
    private int prix;

    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }
}

package com.sovereign.mystation.model;

import javax.persistence.*;

@Entity
@Table(name = "departement")
public class Departement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nom")
    private String nom;
    @Column(name = "localite")
    private String localite;


    @JoinColumn(name = "regionid",nullable = false)
    private int regionid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getRegionid() {
        return regionid;
    }

    public void setRegionid(int region) {
        this.regionid = region;
    }

    public String getLocalite() {
        return localite;
    }

    public void setLocalite(String localite) {
        this.localite = localite;
    }
}

package com.sovereign.mystation.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class GabBanque implements Serializable {

    @Column(name = "nom")
    private String nom;
    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }
}

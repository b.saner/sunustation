package com.sovereign.mystation.model;

import javax.persistence.*;


@Entity
@Table(name = "electrique")

public class Electrique {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;
    @Column(name = "volts", columnDefinition = "varchar(255) default '240'")
    private String volts;
    @Column(name = "verification", columnDefinition = "integer default 3")
    private int verification;
    @Column(name = "borne", columnDefinition = "varchar(255) default 'niveau 2'")
    private String borne;
    @Column(name = "charge", columnDefinition = "varchar(255) default 'Entre 2h à 4h'")
    private String charge;
    @Column(name = "prix", columnDefinition = "integer default 775")
    private int prix;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatut() { return statut;}

    public void setStatut(int statut) { this.statut = statut;}

    public String getVolts() {
        return volts;
    }

    public void setVolts(String volts) {
        this.volts = volts;
    }

    public int getVerification() {
        return verification;
    }

    public void setVerification(int verification) {
        this.verification = verification;
    }

    public String getBorne() {
        return borne;
    }

    public void setBorne(String borne) {
        this.borne = borne;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }
}

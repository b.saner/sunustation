package com.sovereign.mystation.model;

import javax.persistence.*;

@Entity
@Table(name = "entretien")
public class Entretien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "vidange", columnDefinition = "integer default 3")
    private int vidange;
    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;
    //gonflage de pneu
    @Column(name = "pneu", columnDefinition = "integer default 3")
    private int pneu;
    @Column(name = "lavage", columnDefinition = "integer default 3")
    private int lavage;
    @Column(name = "verification", columnDefinition = "integer default 3")
    private int verification;
    @Column(name = "climatisation", columnDefinition = "integer default 3")
    private int climatisation;
    @Column(name = "frein", columnDefinition = "integer default 3")
    private int frein;
    @Column(name = "ventepneu", columnDefinition = "integer default 3")
    private int ventepneu;
    @Column(name = "parebrise", columnDefinition = "integer default 3")
    private int parebrise;
    @Column(name = "horaire")
    private String horaire;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVidange() {
        return vidange;
    }

    public void setVidange(int vidange) {
        this.vidange = vidange;
    }

    public int getStatut() {    return statut; }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getPneu() {
        return pneu;
    }

    public void setPneu(int pneu) {
        this.pneu = pneu;
    }

    public int getLavage() {
        return lavage;
    }

    public void setLavage(int lavage) {
        this.lavage = lavage;
    }

    public int getVerification() {
        return verification;
    }

    public void setVerification(int verification) {
        this.verification = verification;
    }

    public int getClimatisation() {
        return climatisation;
    }

    public void setClimatisation(int climatisation) {
        this.climatisation = climatisation;
    }

    public int getFrein() {
        return frein;
    }

    public void setFrein(int frein) {
        this.frein = frein;
    }

    public int getVentepneu() {
        return ventepneu;
    }

    public void setVentepneu(int ventepneu) {
        this.ventepneu = ventepneu;
    }

    public String getHoraire() {
        return horaire;
    }

    public int getParebrise() {
        return parebrise;
    }

    public void setParebrise(int parebrise) {
        this.parebrise = parebrise;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

}

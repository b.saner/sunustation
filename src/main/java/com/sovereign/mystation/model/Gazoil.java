package com.sovereign.mystation.model;

import javax.persistence.*;

@Entity
@Table(name = "gazoil")
public class Gazoil {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "prix", columnDefinition = "integer default 650")
    private int prix;
    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }
}

package com.sovereign.mystation.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "culte")
public class Culte {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "statut",columnDefinition = "integer default 3")
    private int statut;
    @Column(name = "mosque",columnDefinition = "integer default 3")
    private int mosque;
    @Column(name = "abulution",columnDefinition = "integer default 3")
    private int abulution;
    @Column(name = "eglise",columnDefinition = "integer default 3")
    private int eglise;
    @Column(name = "photos")
    @ElementCollection
    private List<String> photos;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getMosque() {
        return mosque;
    }

    public void setMosque(int mosque) {
        this.mosque = mosque;
    }

    public int getAbulution() {
        return abulution;
    }

    public void setAbulution(int abulution) {
        this.abulution = abulution;
    }

    public int getEglise() {
        return eglise;
    }

    public void setEglise(int eglise) {
        this.eglise = eglise;
    }

}

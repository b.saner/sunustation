package com.sovereign.mystation.model;

import javax.persistence.*;

@Entity
@Table(name = "superette")
public class Superette {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "nom")
    private String nom;

    @Column(name = "nomrestaurant")
    private String nomrestaurant;

    @Column(name = "nomfastfood")
    private String nomfastfood;

    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;
    @Column(name = "gab", columnDefinition = "integer default 3")
    private int gab;
    @Column(name = "fastfood", columnDefinition = "integer default 3")
    private int fastfood;
    @Column(name = "cafeteria", columnDefinition = "integer default 3")
    private int cafeteria;
    @Column(name = "boulangerie", columnDefinition = "integer default 3")
    private int boulangerie;
    @Column(name = "patisserie", columnDefinition = "integer default 3")
    private int patisserie;
    @Column(name = "restaurant", columnDefinition = "integer default 3")
    private int restaurant;
    @Column(name = "superette", columnDefinition = "integer default 3")
    private int superette;
    // Expresso
    @Column(name = "emoney", columnDefinition = "integer default 3")
    private int emoney;
    @Column(name = "orangemoney", columnDefinition = "integer default 3")
    private int orangemoney;
    @Column(name = "wave", columnDefinition = "integer default 3")
    private int wave;
    @Column(name = "wizall", columnDefinition = "integer default 3")
    private int wizall;
    @Column(name = "elmoney", columnDefinition = "integer default 3")
    private int elmoney;
    @Column(name = "proximo", columnDefinition = "integer default 3")
    private int proximo;
    @Column(name = "freemoney", columnDefinition = "integer default 3")
    private int freemoney;
    @Column(name = "sonatel", columnDefinition = "integer default 3")
    private int sonatel;
    @Column(name = "orange", columnDefinition = "integer default 3")
    private int orange;
    @Column(name = "seneau", columnDefinition = "integer default 3")
    private int seneau;
    @Column(name = "senelec", columnDefinition = "integer default 3")
    private int senelec;
    @Column(name = "canal", columnDefinition = "integer default 3")
    private int canal;
    @Column(name = "woyofal", columnDefinition = "integer default 3")
    private int woyofal;
    @Column(name = "rapido", columnDefinition = "integer default 3")
    private int rapido;
    @Column(name = "wari", columnDefinition = "integer default 3")
    private int wari;
    @Column(name = "westerunion", columnDefinition = "integer default 3")
    private int westerunion;
    @Column(name = "moneygram", columnDefinition = "integer default 3")
    private int moneygram;
    @Column(name = "yup", columnDefinition = "integer default 3")
    private int yup;
    @Column(name = "postemoney", columnDefinition = "integer default 3")
    private int postemoney;
    @Column(name = "autre", columnDefinition = "integer default 3")
    private int autre;
    @Column(name = "ria", columnDefinition = "integer default 3")
    private int ria;
    //wc
    @Column(name = "toilette", columnDefinition = "integer default 3")
    private int toilette;
    //wc homme
    @Column(name = "wch", columnDefinition = "integer default 3")
    private int wch;
    //wc femme
    @Column(name = "wcf", columnDefinition = "integer default 3")
    private int wcf;
    //wc unique
    @Column(name = "wcu", columnDefinition = "integer default 3")
    private int wcfu;
    // espace bebe
    @Column(name = "espacebebe", columnDefinition = "integer default 3")
    private int espacebebe;

    // transfert d'argent

   @Column(name = "tmoney", columnDefinition = "integer default 3")
    private int tmoney;

    //  payement facture;
    @Column(name = "pmoney", columnDefinition = "integer default 3")
    private int pmoney;

    //  moyens facture;
    @Column(name = "cmoney", columnDefinition = "integer default 1")
    private int cmoney;

  //  moyens facture;
    @Column(name = "cash", columnDefinition = "integer default 1")
    private int cash;

  //  moyens facture;
    @Column(name = "carte", columnDefinition = "integer default 3")
    private int carte;

 //  moyens facture;
    @Column(name = "kalpe", columnDefinition = "integer default 3")
    private int kalpe;

    //  moyens facture;
    @Column(name = "ctotal", columnDefinition = "integer default 3")
    private int ctotal;

    //  moyens facture;
    @Column(name = "celton", columnDefinition = "integer default 3")
    private int celton;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getStatut() {return statut;}

    public void setStatut(int statut) {this.statut = statut;}


    public int getSuperette() {
        return superette;
    }

    public void setSuperette(int superette) {
        this.superette = superette;
    }

    public int getEmoney() {
        return emoney;
    }

    public void setEmoney(int emoney) {
        this.emoney = emoney;
    }

    public int getOrangemoney() {
        return orangemoney;
    }

    public void setOrangemoney(int orangemoney) {
        this.orangemoney = orangemoney;
    }

    public int getWave() {
        return wave;
    }

    public void setWave(int wave) {
        this.wave = wave;
    }

    public int getWizall() {
        return wizall;
    }

    public void setWizall(int wizall) {
        this.wizall = wizall;
    }

    public int getElmoney() {
        return elmoney;
    }

    public void setElmoney(int elmoney) {
        this.elmoney = elmoney;
    }

    public int getProximo() {
        return proximo;
    }

    public void setProximo(int proximo) {
        this.proximo = proximo;
    }

    public int getFreemoney() {
        return freemoney;
    }

    public void setFreemoney(int freemoney) {
        this.freemoney = freemoney;
    }

    public int getWoyofal() {
        return woyofal;
    }

    public void setWoyofal(int woyofal) {
        this.woyofal = woyofal;
    }

    public int getRapido() {
        return rapido;
    }

    public void setRapido(int rapido) {
        this.rapido = rapido;
    }

    public int getWari() {
        return wari;
    }

    public void setWari(int wari) {
        this.wari = wari;
    }

    public int getWesterunion() {
        return westerunion;
    }

    public void setWesterunion(int westerunion) {
        this.westerunion = westerunion;
    }

    public int getMoneygram() {
        return moneygram;
    }

    public void setMoneygram(int moneygram) {
        this.moneygram = moneygram;
    }

    public int getRia() {
        return ria;
    }

    public void setRia(int ria) {
        this.ria = ria;
    }

    public int getGab() {
        return gab;
    }

    public void setGab(int gab) {
        this.gab = gab;
    }

    public int getFastfood() {
        return fastfood;
    }

    public void setFastfood(int fastfood) {
        this.fastfood = fastfood;
    }

    public int getCafeteria() {
        return cafeteria;
    }

    public void setCafeteria(int cafeteria) {
        this.cafeteria = cafeteria;
    }

    public int getBoulangerie() {
        return boulangerie;
    }

    public void setBoulangerie(int boulangerie) {
        this.boulangerie = boulangerie;
    }

    public int getPatisserie() {
        return patisserie;
    }

    public void setPatisserie(int patisserie) {
        this.patisserie = patisserie;
    }

    public int getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(int restaurant) {
        this.restaurant = restaurant;
    }

    public int getSonatel() {
        return sonatel;
    }

    public void setSonatel(int sonatel) {
        this.sonatel = sonatel;
    }

    public int getOrange() {
        return orange;
    }

    public void setOrange(int orange) {
        this.orange = orange;
    }

    public int getSeneau() {
        return seneau;
    }

    public void setSeneau(int seneau) {
        this.seneau = seneau;
    }

    public int getCanal() {
        return canal;
    }

    public void setCanal(int canal) {
        this.canal = canal;
    }

    public int getYup() {
        return yup;
    }

    public void setYup(int yup) {
        this.yup = yup;
    }

    public int getPostemoney() {
        return postemoney;
    }

    public void setPostemoney(int postemoney) {
        this.postemoney = postemoney;
    }

    public int getAutre() {
        return autre;
    }

    public void setAutre(int autre) {
        this.autre = autre;
    }

    public int getToilette() {
        return toilette;
    }

    public void setToilette(int toilette) {
        this.toilette = toilette;
    }

    public int getWch() {
        return wch;
    }

    public void setWch(int wch) {
        this.wch = wch;
    }

    public int getWcf() {
        return wcf;
    }

    public void setWcf(int wcf) {
        this.wcf = wcf;
    }

    public int getWcfu() {
        return wcfu;
    }

    public void setWcfu(int wcfu) {
        this.wcfu = wcfu;
    }

    public int getEspacebebe() {
        return espacebebe;
    }

    public void setEspacebebe(int espacebebe) {
        this.espacebebe = espacebebe;
    }

    public int getSenelec() {
        return senelec;
    }

    public void setSenelec(int senelec) {
        senelec = senelec;
    }

    public String getNomrestaurant() {
        return nomrestaurant;
    }

    public void setNomrestaurant(String nomrestaurant) {
        this.nomrestaurant = nomrestaurant;
    }

    public String getNomfastfood() {
        return nomfastfood;
    }

    public void setNomfastfood(String nomfastfood) {
        this.nomfastfood = nomfastfood;
    }

    public int getTmoney() {
        return tmoney;
    }

    public void setTmoney(int tmoney) {
        this.tmoney = tmoney;
    }

    public int getPmoney() {
        return pmoney;
    }

    public void setPmoney(int pmoney) {
        this.pmoney = pmoney;
    }

    public int getCmoney() {
        return cmoney;
    }

    public void setCmoney(int cmoney) {
        this.cmoney = cmoney;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getCarte() {
        return carte;
    }

    public void setCarte(int carte) {
        this.carte = carte;
    }

    public int getKalpe() {
        return kalpe;
    }

    public void setKalpe(int kalpe) {
        this.kalpe = kalpe;
    }

    public int getCtotal() {
        return ctotal;
    }

    public void setCtotal(int ctotal) {
        this.ctotal = ctotal;
    }

    public int getCelton() {
        return celton;
    }

    public void setCelton(int celton) {
        this.celton = celton;
    }
}

package com.sovereign.mystation.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "airederepos")
public class Airederepos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "statut",columnDefinition = "integer default 3")
    private int statut;
    @Column(name = "airedejeu",columnDefinition = "integer default 3")
    private int airedejeu;
    @Column(name = "airedepicnic",columnDefinition = "integer default 3")
    private int airedepicnic;
    @Column(name = "espaceenfant",columnDefinition = "integer default 3")
    private int espaceenfant;
    @Column(name = "eau",columnDefinition = "integer default 3")
    private int eau;
    //parking pour vehicules legers
    @Column(name = "pvl",columnDefinition = "integer default 3")
    private int pvl;
    //parking pour poids lourds
    @Column(name = "ppl",columnDefinition = "integer default 3")
    private int ppl;
    //parking pour les caravanes
    @Column(name = "ppc",columnDefinition = "integer default 3")
    private int ppc;
    //parking pour les handicapées
    @Column(name = "handicape",columnDefinition = "integer default 3")
    private int handicape;
    //station de gonflage pneus
    @Column(name = "gonflage",columnDefinition = "integer default 3")
    private int gonflage;
    @Column(name = "photos")
    @ElementCollection
    private List<String> photos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getAiredejeu() {
        return airedejeu;
    }

    public void setAiredejeu(int airedejeu) {
        this.airedejeu = airedejeu;
    }

    public int getAiredepicnic() {
        return airedepicnic;
    }

    public void setAiredepicnic(int airedepicnic) {
        this.airedepicnic = airedepicnic;
    }

    public int getEspaceenfant() {
        return espaceenfant;
    }

    public void setEspaceenfant(int espaceenfant) {
        this.espaceenfant = espaceenfant;
    }

    public int getEau() {
        return eau;
    }

    public void setEau(int eau) {
        this.eau = eau;
    }

    public int getPvl() {
        return pvl;
    }

    public void setPvl(int pvl) {
        this.pvl = pvl;
    }

    public int getPpl() {
        return ppl;
    }

    public void setPpl(int ppl) {
        this.ppl = ppl;
    }

    public int getPpc() {
        return ppc;
    }

    public void setPpc(int ppc) {
        this.ppc = ppc;
    }

    public int getHandicape() {
        return handicape;
    }

    public void setHandicape(int handicape) {
        this.handicape = handicape;
    }

    public int getGonflage() {
        return gonflage;
    }

    public void setGonflage(int gonflage) {
        this.gonflage = gonflage;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }
}

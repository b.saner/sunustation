package com.sovereign.mystation.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "gab")
public class Gab {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;
    @ElementCollection
    @CollectionTable(name = "gab_banque", joinColumns = @JoinColumn(name = "gab_id"))
    private List<GabBanque> gabbanque;
//    @Column(name = "ecobank")
//    private int ecobank;
//    @Column(name = "sg")
//    private int sg;
//    @Column(name = "vigile")
//    private int vigile;
//    @Column(name = "camera")
//    private int camera;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public List<GabBanque> getGabbanque() {
        return gabbanque;
    }

    public void setGabbanque(List<GabBanque> gabbanque) {
        this.gabbanque = gabbanque;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }
}


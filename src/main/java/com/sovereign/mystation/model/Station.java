package com.sovereign.mystation.model;

import javax.persistence.*;

@Entity
@Table(name = "station")
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "latitude")
    private String latitude;
    @Column(name = "longitude")
    private String longitude;
    @Column(name = "nom")
    private String nom;
    @Column(name = "tel")
    private String tel;
    @Column(name = "logo")
    private String logo;
    @Column(name = "mail")
    private String mail;
    @Column(name = "departement")
    private String departement;
    @Column(name = "region")
    private String region;
    @Column(name = "publier", columnDefinition = "boolean default true")
    private boolean publier;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "airedereposid", columnDefinition = "integer default 3")
    private Airederepos airederepos;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "culteid", columnDefinition = "integer default 3")
    private Culte culte;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "electriqueid", columnDefinition = "integer default 3")
    private Electrique electrique;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "gplid", columnDefinition = "integer default 3")
    private Gpl gpl;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "entretienid", columnDefinition = "integer default 3")
    private Entretien entretien;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "essenceid", columnDefinition = "integer default 3")
    private Essence essence;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "gabid", columnDefinition = "integer default 3")
    private Gab gab;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "gazid", columnDefinition = "integer default 3")
    private Gaz gaz;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "gazoilid", columnDefinition = "integer default 3")
    private Gazoil gazoil;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "toiletteid", columnDefinition = "integer default 3")
    private Toilette toilette;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "superetteid", columnDefinition = "integer default 3")
    private Superette superette;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "managerid", columnDefinition = "integer default 3")
    private Manager manager;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean isPublier() {
        return publier;
    }

    public void setPublier(boolean publier) {
        this.publier = publier;
    }

    public Airederepos getAirederepos() {
        return airederepos;
    }

    public void setAirederepos(Airederepos airederepos) {
        this.airederepos = airederepos;
    }

    public Culte getCulte() {
        return culte;
    }

    public void setCulte(Culte culte) {
        this.culte = culte;
    }

    public Electrique getElectrique() {
        return electrique;
    }

    public void setElectrique(Electrique electrique) {
        this.electrique = electrique;
    }

    public Entretien getEntretien() {
        return entretien;
    }

    public void setEntretien(Entretien entretien) {
        this.entretien = entretien;
    }

    public Essence getEssence() {
        return essence;
    }

    public void setEssence(Essence essence) {
        this.essence = essence;
    }

    public Gab getGab() {
        return gab;
    }

    public void setGab(Gab gab) {
        this.gab = gab;
    }

    public Gaz getGaz() {
        return gaz;
    }

    public void setGaz(Gaz gaz) {
        this.gaz = gaz;
    }

    public Gazoil getGazoil() {
        return gazoil;
    }

    public void setGazoil(Gazoil gazoil) {
        this.gazoil = gazoil;
    }

    public Gpl getGpl() {return gpl;}

    public void setGpl(Gpl gpl) {this.gpl = gpl;}

    public Superette getSuperette() {
        return superette;
    }

    public void setSuperette(Superette superette) {
        this.superette = superette;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Toilette getToilette() {
        return toilette;
    }

    public void setToilette(Toilette toilette) {
        this.toilette = toilette;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }
}

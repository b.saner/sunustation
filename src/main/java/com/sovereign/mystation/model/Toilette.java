package com.sovereign.mystation.model;

import javax.persistence.*;


@Entity
@Table(name = "toilette")
public class Toilette {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "statut", columnDefinition = "integer default 3")
    private int statut;
    //wc homme
    @Column(name = "wch", columnDefinition = "integer default 3")
    private int wch;
    //wc femme
    @Column(name = "wcf", columnDefinition = "integer default 3")
    private int wcf;
    //wc unique
    @Column(name = "wcu", columnDefinition = "integer default 3")
    private int wcfu;
    // espace bebe
    @Column(name = "espacebebe", columnDefinition = "integer default 3")
    private int espacebebe;
    @Column(name = "chaiseanglaise", columnDefinition = "integer default 3")
    private int chaiseanglaise;
    @Column(name = "chaiseturc", columnDefinition = "integer default 3")
    private int chaiseturc;
    @Column(name = "lavabo", columnDefinition = "integer default 3")
    private int lavabo;
    @Column(name = "douche", columnDefinition = "integer default 3")
    private int douche;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public int getWch() {
        return wch;
    }

    public void setWch(int wch) {
        this.wch = wch;
    }

    public int getWcf() {
        return wcf;
    }

    public void setWcf(int wcf) {
        this.wcf = wcf;
    }

    public int getWcfu() {
        return wcfu;
    }

    public void setWcfu(int wcfu) {
        this.wcfu = wcfu;
    }

    public int getEspacebebe() {
        return espacebebe;
    }

    public void setEspacebebe(int espacebbebe) {
        this.espacebebe = espacebebe;
    }

    public int getChaiseanglaise() {
        return chaiseanglaise;
    }

    public void setChaiseanglaise(int chaiseanglaise) {
        this.chaiseanglaise = chaiseanglaise;
    }

    public int getChaiseturc() {
        return chaiseturc;
    }

    public void setChaiseturc(int chaiseturc) {
        this.chaiseturc = chaiseturc;
    }

    public int getLavabo() {
        return lavabo;
    }

    public void setLavabo(int lavabo) {
        this.lavabo = lavabo;
    }

    public int getDouche() {
        return douche;
    }

    public void setDouche(int douche) {
        this.douche = douche;
    }

}

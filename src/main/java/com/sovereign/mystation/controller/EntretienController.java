package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Entretien;
import com.sovereign.mystation.repository.EntretienDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/entretien")

public class EntretienController {
    @Autowired
    private EntretienDao entretienDao;

    @GetMapping
    public List<Entretien> getallEntretien() {
        return this.entretienDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newEntretien (@RequestBody Entretien newEntretien) {
        return ResponseEntity.ok( entretienDao.save(newEntretien));
    }
    // get one entretien
    @GetMapping("/{id}")
    public Entretien getById(@PathVariable(value = "id") int id) {
        return this.entretienDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Entretien entretien, @PathVariable int id) {

        try {
            Entretien existEntretien  = getById(id);
            this.entretienDao.save(entretien);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.entretienDao.deleteById(id);
    }

}

package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Toilette;
import com.sovereign.mystation.repository.ToiletteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/toilette")

public class ToiletteController {
    @Autowired
    private ToiletteDao toilettedao;

    @GetMapping
    public List<Toilette> getallToilette() {
        return this.toilettedao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newToilette (@RequestBody Toilette newToilette) {
        return ResponseEntity.ok( toilettedao.save(newToilette));
    }
    // get one toilette
    @GetMapping("/{id}")
    public Toilette getById(@PathVariable(value = "id") int id) {
        return this.toilettedao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Toilette toilette, @PathVariable int id) {

        try {
            Toilette existToilette  = getById(id);
            this.toilettedao.save(toilette);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.toilettedao.deleteById(id);
    }

}

package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Culte;
import com.sovereign.mystation.repository.CulteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/culte")

public class CulteController {
    @Autowired
    private CulteDao culteDao;

    @GetMapping
    public List<Culte> getallCulte() {
        return this.culteDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newCulte (@RequestBody Culte newCulte) {
        return ResponseEntity.ok( culteDao.save(newCulte));
    }
    // get one culte
    @GetMapping("/{id}")
    public Culte getById(@PathVariable(value = "id") int id) {
        return this.culteDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Culte culte, @PathVariable int id) {

        try {
            Culte existCulte  = getById(id);
            this.culteDao.save(culte);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.culteDao.deleteById(id);
    }

}

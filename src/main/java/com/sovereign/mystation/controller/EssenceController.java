package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Essence;
import com.sovereign.mystation.repository.EssenceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/essence")

public class EssenceController {
    @Autowired
    private EssenceDao essenceDao;

    @GetMapping
    public List<Essence> getallEssence() {
        return this.essenceDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newEssence (@RequestBody Essence newEssence) {
        return ResponseEntity.ok( essenceDao.save(newEssence));
    }
    // get one essence
    @GetMapping("/{id}")
    public Essence getById(@PathVariable(value = "id") int id) {
        return this.essenceDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Essence essence, @PathVariable int id) {

        try {
            Essence existEssence  = getById(id);
            this.essenceDao.save(essence);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.essenceDao.deleteById(id);
    }

}

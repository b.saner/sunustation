package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Gazoil;
import com.sovereign.mystation.repository.GazoilDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/gazoil")

public class GazoilController {
    @Autowired
    private GazoilDao gazoilDao;

    @GetMapping
    public List<Gazoil> getallGazoil() {
        return this.gazoilDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newGazoil (@RequestBody Gazoil newGazoil) {
        return ResponseEntity.ok( gazoilDao.save(newGazoil));
    }
    // get one gazoil
    @GetMapping("/{id}")
    public Gazoil getById(@PathVariable(value = "id") int id) {
        return this.gazoilDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Gazoil gazoil, @PathVariable int id) {

        try {
            Gazoil existGazoil  = getById(id);
            this.gazoilDao.save(gazoil);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.gazoilDao.deleteById(id);
    }

}

package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Station;
import com.sovereign.mystation.repository.StationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/station")

public class StationController {
    @Autowired
    private StationDao stationDao;

    @GetMapping
    public List<Station> getallStation() {
        return this.stationDao.findAllByPublierNot(false);
    }

    @PostMapping
    public ResponseEntity<?> newStation (@RequestBody Station newStation) {
        return ResponseEntity.ok( stationDao.save(newStation));
    }
    // get one station
    @GetMapping("/{id}")
    public Station getById(@PathVariable(value = "id") int id) {
        return this.stationDao.findById(id);
    }

  // search  stations
    @GetMapping("/search/{nom}")
    public List<Station> Searchstation(@PathVariable(value = "nom") String nom) {
        return this.stationDao.findByNomIsContaining(nom);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Station station, @PathVariable int id) {

        try {
            Station existStation  = getById(id);
            this.stationDao.save(station);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.stationDao.deleteById(id);
    }

}

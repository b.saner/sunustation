package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Departement;
import com.sovereign.mystation.repository.DepartementDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/departement")

public class DepartementController {
    @Autowired
    private DepartementDao departementDao;

    @GetMapping
    public List<Departement> getallDepartement() {
        return this.departementDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newDepartement (@RequestBody Departement newDepartement) {
        return ResponseEntity.ok( departementDao.save(newDepartement));
    }

    // get one departement
    @GetMapping("/{id}")
    public Departement getById(@PathVariable(value = "id") int id) {
        return this.departementDao.findById(id);
    }


    // get one departement by region
    @GetMapping("/region/{id}")
    public List<Departement> getByRegion(@PathVariable(value = "id") int id) {
        return this.departementDao.findByRegionid(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Departement departement, @PathVariable int id) {

        try {
            Departement existDepartement  = getById(id);
            this.departementDao.save(departement);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.departementDao.deleteById(id);
    }


}

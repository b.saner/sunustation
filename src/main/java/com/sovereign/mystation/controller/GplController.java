package com.sovereign.mystation.controller;


import com.sovereign.mystation.model.Gpl;
import com.sovereign.mystation.repository.GplDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/gpl")
public class GplController {
    @Autowired
    GplDao gplDao;

    @GetMapping
    public List<Gpl> getallGpl() {
        return this.gplDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newGpl (@RequestBody Gpl newGpl) {
        return ResponseEntity.ok( gplDao.save(newGpl));
    }
    // get one gpl
    @GetMapping("/{id}")
    public Gpl getById(@PathVariable(value = "id") int id) {
        return this.gplDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Gpl gpl, @PathVariable int id) {

        try {
            Gpl existGpl  = getById(id);
            this.gplDao.save(gpl);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.gplDao.deleteById(id);
    }

}

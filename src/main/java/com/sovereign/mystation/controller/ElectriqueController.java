package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Electrique;
import com.sovereign.mystation.repository.ElectriqueDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/electrique")

public class ElectriqueController {
    @Autowired
    private ElectriqueDao electriqueDao;

    @GetMapping
    public List<Electrique> getallElectrique() {
        return this.electriqueDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newElectrique (@RequestBody Electrique newElectrique) {
        return ResponseEntity.ok( electriqueDao.save(newElectrique));
    }
    // get one electrique
    @GetMapping("/{id}")
    public Electrique getById(@PathVariable(value = "id") int id) {
        return this.electriqueDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Electrique electrique, @PathVariable int id) {

        try {
            Electrique existElectrique  = getById(id);
            this.electriqueDao.save(electrique);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.electriqueDao.deleteById(id);
    }

}

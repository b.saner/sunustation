package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Superette;
import com.sovereign.mystation.repository.SuperetteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/superette")

public class SuperetteController {
    @Autowired
    private SuperetteDao superetteDao;

    @GetMapping
    public List<Superette> getallSuperette() {
        return this.superetteDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newSuperette (@RequestBody Superette newSuperette) {
        return ResponseEntity.ok( superetteDao.save(newSuperette));
    }
    // get one superette
    @GetMapping("/{id}")
    public Superette getById(@PathVariable(value = "id") int id) {
        return this.superetteDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Superette superette, @PathVariable int id) {

        try {
            Superette existSuperette  = getById(id);
            this.superetteDao.save(superette);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.superetteDao.deleteById(id);
    }

}

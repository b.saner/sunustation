package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Airederepos;
import com.sovereign.mystation.repository.AiredereposDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/airederepos")

public class AiredereposController {
    @Autowired
    private AiredereposDao airedereposDao;

    @GetMapping
    public List<Airederepos> getallAirederepos() {
        return this.airedereposDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newAirederepos (@RequestBody Airederepos newAirederepos) {
        return ResponseEntity.ok( airedereposDao.save(newAirederepos));
    }
    // get one airederepos
    @GetMapping("/{id}")
    public Airederepos getById(@PathVariable(value = "id") int id) {
        return this.airedereposDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Airederepos airederepos, @PathVariable int id) {

        try {
            Airederepos existAirederepos  = getById(id);
            this.airedereposDao.save(airederepos);
            return new ResponseEntity<>( HttpStatus.OK);
        }

        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.airedereposDao.deleteById(id);
    }

}

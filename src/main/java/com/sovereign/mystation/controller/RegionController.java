package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Region;
import com.sovereign.mystation.repository.RegionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/region")

public class RegionController {
    @Autowired
    private RegionDao regionDao;

    @GetMapping
    public List<Region> getallRegion() {
        return this.regionDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newrRegion (@RequestBody Region newRegion) {
        return ResponseEntity.ok( regionDao.save(newRegion));
    }
    // get one statut
    @GetMapping("/{id}")
    public Region getById(@PathVariable(value = "id") int id) {
        return this.regionDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Region region, @PathVariable int id) {

        try {
            Region existRegion  = getById(id);
            this.regionDao.save(region);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.regionDao.deleteById(id);
    }


}

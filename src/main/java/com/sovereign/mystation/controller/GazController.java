package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Gaz;
import com.sovereign.mystation.repository.GazDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/gaz")

public class GazController {
    @Autowired
    private GazDao gazDao;

    @GetMapping
    public List<Gaz> getallGaz() {
        return this.gazDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newGaz (@RequestBody Gaz newGaz) {
        return ResponseEntity.ok( gazDao.save(newGaz));
    }
    // get one gaz
    @GetMapping("/{id}")
    public Gaz getById(@PathVariable(value = "id") int id) {
        return this.gazDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Gaz gaz, @PathVariable int id) {

        try {
            Gaz existGaz  = getById(id);
            this.gazDao.save(gaz);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.gazDao.deleteById(id);
    }

}

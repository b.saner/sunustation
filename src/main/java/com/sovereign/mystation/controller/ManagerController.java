package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Manager;
import com.sovereign.mystation.repository.ManagerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/manager")

public class ManagerController {
    @Autowired
    private ManagerDao managerDao;

    @GetMapping
    public List<Manager> getallManager() {
        return this.managerDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newManager (@RequestBody Manager newManager) {
        return ResponseEntity.ok( managerDao.save(newManager));
    }
    // get one manager
    @GetMapping("/{id}")
    public Manager getById(@PathVariable(value = "id") int id) {
        return this.managerDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Manager manager, @PathVariable int id) {

        try {
            Manager existManager  = getById(id);
            this.managerDao.save(manager);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.managerDao.deleteById(id);
    }

}

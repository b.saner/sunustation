package com.sovereign.mystation.controller;

import com.sovereign.mystation.model.Gab;
import com.sovereign.mystation.repository.GabDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("/api/gab")

public class GabController {
    @Autowired
    private GabDao gabDao;

    @GetMapping
    public List<Gab> getallGab() {
        return this.gabDao.findAll();
    }

    @PostMapping
    public ResponseEntity<?> newGab (@RequestBody Gab newGab) {
        return ResponseEntity.ok( gabDao.save(newGab));
    }
    // get one gab
    @GetMapping("/{id}")
    public Gab getById(@PathVariable(value = "id") int id) {
        return this.gabDao.findById(id);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Gab gab, @PathVariable int id) {

        try {
            Gab existGab  = getById(id);
            this.gabDao.save(gab);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (NoSuchElementException e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        this.gabDao.deleteById(id);
    }

}

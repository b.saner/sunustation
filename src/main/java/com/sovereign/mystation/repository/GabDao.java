package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Gab;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GabDao extends JpaRepository<Gab,Integer> {
public Gab findById(int id);
}

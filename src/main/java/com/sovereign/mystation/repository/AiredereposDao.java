package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Airederepos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AiredereposDao extends JpaRepository<Airederepos,Integer> {
public Airederepos findById(int id);
}

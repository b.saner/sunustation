package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Entretien;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntretienDao extends JpaRepository<Entretien,Integer> {
public Entretien findById(int id);
}

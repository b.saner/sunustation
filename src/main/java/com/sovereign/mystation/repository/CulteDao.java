package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Culte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CulteDao extends JpaRepository<Culte,Integer> {
public Culte findById(int id);
}

package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionDao extends JpaRepository<Region,Integer> {
    public Region findById(int id);
}

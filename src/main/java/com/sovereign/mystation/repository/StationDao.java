package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StationDao extends JpaRepository<Station,Integer> {
public Station findById(int id);
public List<Station> findAllByPublierNot(boolean id);
    public List<Station> findByNomIsContaining(String nom);

}

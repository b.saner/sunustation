package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Gaz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GazDao extends JpaRepository<Gaz,Integer> {
public Gaz findById(int id);
}

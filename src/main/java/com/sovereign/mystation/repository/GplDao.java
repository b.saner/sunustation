package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Gpl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GplDao extends JpaRepository<Gpl,Integer> {
    public Gpl findById(int id);
}

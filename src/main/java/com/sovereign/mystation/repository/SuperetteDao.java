package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Superette;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuperetteDao extends JpaRepository<Superette,Integer> {
public Superette findById(int id);
}

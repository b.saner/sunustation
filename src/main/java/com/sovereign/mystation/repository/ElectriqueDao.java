package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Electrique;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ElectriqueDao extends JpaRepository<Electrique,Integer> {
public Electrique findById(int id);
}

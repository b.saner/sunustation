package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Essence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EssenceDao extends JpaRepository<Essence,Integer> {
public Essence findById(int id);
}

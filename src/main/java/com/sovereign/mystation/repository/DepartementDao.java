package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Departement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartementDao extends JpaRepository<Departement,Integer> {
    public Departement findById(int id);

    public List<Departement> findByRegionid(int id);
}

package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerDao extends JpaRepository<Manager,Integer> {
public Manager findById(int id);
}

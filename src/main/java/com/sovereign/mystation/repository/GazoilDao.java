package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Gazoil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GazoilDao extends JpaRepository<Gazoil,Integer> {
public Gazoil findById(int id);
}

package com.sovereign.mystation.repository;

import com.sovereign.mystation.model.Toilette;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToiletteDao extends JpaRepository<Toilette,Integer> {
public Toilette findById(int id);
}
